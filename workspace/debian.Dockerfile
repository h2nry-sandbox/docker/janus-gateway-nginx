################ global ################
ARG LIBSRTP_VERSION="2.6.0"
ARG LIBNICE_VERSION="0.1.12"
ARG JANUS_VERSION="v1.2.3"
ARG JANUS_HOME="/opt/janus"

################ build stage ################
FROM debian:bullseye-slim AS builder
ARG LIBSRTP_VERSION
ARG LIBNICE_VERSION
ARG JANUS_VERSION
ARG JANUS_HOME

#
# Default
#
RUN apt-get update -y \
  && apt-get install -y apt-utils

#
# janus-gateway's official requirements
#   => https://github.com/meetecho/janus-gateway/tree/v1.2.2?tab=readme-ov-file#dependencies
#
RUN apt-get install -y \
  libmicrohttpd-dev libjansson-dev \
  libssl-dev libsofia-sip-ua-dev libglib2.0-dev \
  libopus-dev libogg-dev libcurl4-openssl-dev liblua5.3-dev \
  libconfig-dev pkg-config libtool automake

# for enable-docs option
RUN apt-get install -y doxygen graphviz

#
# Custom Requirements
#
RUN apt-get install -y \
  wget git autoconf sudo gtk-doc-tools pip

#
# Default
#
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*

#
# Install libnice
#
RUN cd /tmp \
  && pip3 install meson ninja \
  && git clone https://gitlab.freedesktop.org/libnice/libnice --branch ${LIBNICE_VERSION} --single-branch \
  && cd libnice \
  && ./autogen.sh \
  && ./configure --prefix=/usr \
  && make \
  && make install

#
# Install libsrtp
#
RUN cd /tmp \
  && wget https://github.com/cisco/libsrtp/archive/v${LIBSRTP_VERSION}.tar.gz \
  && tar xfv v${LIBSRTP_VERSION}.tar.gz \
  && cd libsrtp-${LIBSRTP_VERSION} \
  && ./configure --prefix=/usr --enable-openssl \
  && make shared_library \
  && make install

#
# Install janus-gateway
#
RUN cd /tmp \
  && git clone https://github.com/meetecho/janus-gateway.git --branch ${JANUS_VERSION} --single-branch \
  && cd janus-gateway \
  && sh autogen.sh \
  && ./configure --prefix=${JANUS_HOME} \
    --enable-docs \
    --disable-websockets \
    --disable-data-channels \
    --disable-rabbitmq \
    --disable-mqtt \
  && make \
  && make install \
  && make configs

################ launch stage ################
FROM debian:bullseye-slim AS launcher
ARG JANUS_HOME

LABEL version="1.0.0"
LABEL maintainer="h2nry"
LABEL author="h2nry <me@h2nry.com>"

EXPOSE 10000-10200/udp
EXPOSE 8188/tcp
EXPOSE 8088/tcp
EXPOSE 8089/tcp
EXPOSE 8889/tcp
EXPOSE 8000/tcp
EXPOSE 7088/tcp
EXPOSE 7089/tcp
EXPOSE 80/tcp
EXPOSE 443/tcp

WORKDIR "/workspace"

#
# Default
#
RUN apt-get update -y \
  && apt-get install -y apt-utils

#
# Install Nginx
#
RUN addgroup --system -gid 110 messagebus \
  && adduser --system --uid 106 --gid 110 --home /var/run/dbus messagebus \
  && adduser --force-badname --system --home /nonexistent --no-create-home --quiet _apt || true

RUN apt-get install -y \
  nginx vim

#
# Install for janus
#
RUN apt-get install -y \
  libmicrohttpd12 libjansson4 \
  libssl1.1 libsofia-sip-ua0 libglib2.0 \
  libopus0 libogg0 libcurl4 liblua5.3-0 \
  libconfig9 libusrsctp1

#
# Default
#
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*

#
# Migrate Artifacts on builder to launcher
#
COPY --from=builder /usr/lib/libsrtp2* /usr/lib/
COPY --from=builder /usr/lib/libnice* /usr/lib/
COPY --from=builder ${JANUS_HOME} ${JANUS_HOME}

#
# Copy assets to workspace
#
COPY ./assets/janus.conf /etc/nginx/conf.d/
COPY ./assets/entrypoint.sh /workspace/

CMD [ "/workspace/entrypoint.sh" ]
