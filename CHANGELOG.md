# 1.0.0 (2024-06-22)

### Bug Fixes

- 프로젝트 초기화 ([6dfe197](https://gitlab.com/h2nry-sandbox/docker/janus-gateway-nginx/commit/6dfe197f16948e7df07256eda308c166a31ae4c0))
