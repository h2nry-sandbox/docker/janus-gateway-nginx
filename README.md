# Janus WebRTC Server with Nginx

### Purpose

Nothing.
I was just annoyed by having to new install WAS(like nginx, apache...) when running the demo for Janus Gateway.
So, I created this to save your time.

I have only built it for Debian now, but i will also provide it based on Fedora.
Please, wait or contribute for it.

### Usage

```bash
$ docker pull h2nry/janus-gateway-nginx:latest
$ docker run -d --name janus \
  -p 80:80 \
  -p 443:443 \
  -p 7089:7089 \
  -p 8000:8000 \
  -p 8889:8889 \
  -p 8089:8089 \
  -p 8088:8088 \
  -p 8188:8188 \
  -p 10000-10200:10000-10200 \
  h2nry/janus-gateway-nginx:latest
$ docker exec -it janus bash
```
